// Copyright 2017 The Gitea Authors. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.

package models

import (
	"fmt"
	"time"

	"gopkg.in/testfixtures.v2"
)

var fixtures *testfixtures.Context

// InitFixtures initialize test fixtures for a test database
//为测试数据库初始化 fixture
func InitFixtures(helper testfixtures.Helper, dir string) (err error) {
	testfixtures.SkipDatabaseNameCheck(true)
	fixtures, err = testfixtures.NewFolder(x.DB().DB, helper, dir)
	return err
}

// LoadFixtures load fixtures for a test database
// 从当前目录的 fixture 文件夹中加载数据
func LoadFixtures() error {
	var err error
	// Database transaction conflicts could occur and result in ROLLBACK
	// As a simple workaround, we just retry 20 times.
	for i := 0; i < 20; i++ {   // 如果数据出现冲突,只尝试 20 次
		err = fixtures.Load()   // 从当前目录的 fixture 文件夹中加载数据
		if err == nil {
			break
		}
		time.Sleep(200 * time.Millisecond)
	}
	if err != nil {
		fmt.Printf("LoadFixtures failed after retries: %v\n", err)
	}
	return err
}
