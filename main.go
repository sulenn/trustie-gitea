// Copyright 2014 The Gogs Authors. All rights reserved.
// Copyright 2016 The Gitea Authors. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.

// Gitea (git with a cup of tea) is a painless self-hosted Git Service.
package main // import "code.gitea.io/gitea"

import (
	"fmt"
	"os"
	"runtime"
	"strings"

	"code.gitea.io/gitea/cmd"
	"code.gitea.io/gitea/modules/log"
	"code.gitea.io/gitea/modules/setting"

	// register supported doc types
	_ "code.gitea.io/gitea/modules/markup/csv"
	_ "code.gitea.io/gitea/modules/markup/markdown"
	_ "code.gitea.io/gitea/modules/markup/orgmode"

	// for embed
	_ "github.com/shurcooL/vfsgen"

	"github.com/urfave/cli"
)

var (
	// Version holds the current Gitea version
	Version = "1.9.0-dev"
	// Tags holds the build tags used
	Tags = ""    //编译标签
	// MakeVersion holds the current Make version if built with make
	MakeVersion = ""   //Make 编译的版本

	originalAppHelpTemplate        = ""  // 原始应用 help 模板 ./gitea --help
	originalCommandHelpTemplate    = ""  // 原始应用的命令 help 模板 ./gitea web --help
	originalSubcommandHelpTemplate = ""  // 原始应用的子命令 help 模板 ./gitea web port --help
)

func init() {     // main.go 文件作为工程主入口。但是由于引用了其它包，所以其它包中的 init 函数会在main.go中Init函数之前先被初始化。其中 setting.go 中
                  // 的 init 函数对 AppWorkPath 变量的初始化会影响好几个关键路径的赋值，进而影响 web 路由的路径是否正确
                  // 这几个关键路径为 setting.CustomPath、setting.CustomConf、setting.AppWorkPath 等
                  // 所以需要在 setting.go 中 init 函数的开始处手动添加 os.Args = []string {"./gitea", "web"}，解决路由路径的问题。
                  // 不然，访问 http://localhost:3000/ 网址， 会出现 html/template: "install" is undefined 的错误
	setting.AppVer = Version   // gitea 应用的版本
	setting.AppBuiltWith = formatBuiltWith()      // 获编译环境, Make 版本 + go 版本 + tag 标签

	// Grab the original help templates
	originalAppHelpTemplate = cli.AppHelpTemplate   // 原始应用 help 模板 ./gitea --help
	originalCommandHelpTemplate = cli.CommandHelpTemplate  // 原始应用的命令 help 模板 ./gitea web --help
	originalSubcommandHelpTemplate = cli.SubcommandHelpTemplate   // 原始应用的子命令 help 模板 ./gitea web port --help
}
//调用 github.com/urfave/cli 包来编写命令行工具
func main() {
	//add by qiubing
	// if os.Args[0] == "/tmp/___go_build_main_go" {       // 强凑， 目的是可以成功从 goland 启动
	// 	os.Args = []string {"./gitea", "web"}
	// }
	os.Args = []string {"./gitea", "web"}
	//add by qiubing
	app := cli.NewApp()  // 初始化应用 app
	app.Name = "Gitea"   //应用名
	app.Usage = "A painless self-hosted Git service"   //应用简介
	app.Description = `By default, gitea will start serving using the webserver with no
arguments - which can alternatively be run by running the subcommand web.`  // 应用描述
	app.Version = Version + formatBuiltWith()   //应用版本号 + 编译环境
	app.Commands = []cli.Command{  // 注册 app 应用拥有的指令
		cmd.CmdWeb,       // web 真实入口
		cmd.CmdServ,
		cmd.CmdHook,
		cmd.CmdDump,
		cmd.CmdCert,
		cmd.CmdAdmin,
		cmd.CmdGenerate,
		cmd.CmdMigrate,
		cmd.CmdKeys,
		cmd.CmdConvert,
	}
	// 调整这些命令至全局配置选项中

	//首先在 context 中设置默认路径和各命令的 help 提示模板
	setting.SetCustomPathAndConf("", "", "")    // 设置定制化文件夹路径和定制化配置文件路径
	setAppHelpTemplates()  // 设置各命令的 help 提示模板

	//fmt.Println("os.Args:",os.Args)
	//fmt.Println("setting.CustomPath:", setting.CustomPath)
	//fmt.Println("setting.CustomConf:", setting.CustomConf)
	//fmt.Println("setting.AppWorkPath:", setting.AppWorkPath)

	// default configuration flags 配置默认的 configuration flags
	defaultFlags := []cli.Flag{   // 定制化文件夹路径
		cli.StringFlag{
			Name:  "custom-path, C",
			Value: setting.CustomPath,
			Usage: "Custom path file path",
		},
		cli.StringFlag{   // 定制化配置文件路径
			Name:  "config, c",
			Value: setting.CustomConf,
			Usage: "Custom configuration file path",
		},
		cli.VersionFlag,
		cli.StringFlag{   // 定制化应用工作路径
			Name:  "work-path, w",
			Value: setting.AppWorkPath,
			Usage: "Set the gitea working path",
		},
	}

	// Set the default to be equivalent to cmdWeb and add the default flags
	app.Flags = append(app.Flags, cmd.CmdWeb.Flags...)   // 将 CmdWeb 子命令中的 Flags 加入 app.Flags 中
	app.Flags = append(app.Flags, defaultFlags...)
	app.Action = cmd.CmdWeb.Action   // cmd.CmdWeb.Action 表示 Web 应用的真正入口: 'runWeb' 方法

	// Add functions to set these paths and these flags to the commands
	app.Before = establishCustomPath
	for i := range app.Commands {   // 遍历每一个命令
		setFlagsAndBeforeOnSubcommands(&app.Commands[i], defaultFlags, establishCustomPath)   // 为每一个命令设置 Flags 和 Before
	}
	err := app.Run(os.Args)    // 整个应用真正的默认入口函数
	if err != nil {
		log.Fatal("Failed to run app with %s: %v", os.Args, err)
	}
}

// 为命令设置 Flags 和 Before
func setFlagsAndBeforeOnSubcommands(command *cli.Command, defaultFlags []cli.Flag, before cli.BeforeFunc) {
	command.Flags = append(command.Flags, defaultFlags...)
	command.Before = establishCustomPath
	for i := range command.Subcommands {  // 遍历命令的子命令
		setFlagsAndBeforeOnSubcommands(&command.Subcommands[i], defaultFlags, before)   //为子命令设置 Flags 和 Before
	}
}

//建立定制化的文件夹路径和定制化配置文件路径,设置各个命令的 help 提示模板. app.run()之后,会根据输入的命令调用该方法来设置全局的值.
func establishCustomPath(ctx *cli.Context) error {
	var providedCustom string
	var providedConf string
	var providedWorkPath string

	currentCtx := ctx
	for {
		if len(providedCustom) != 0 && len(providedConf) != 0 && len(providedWorkPath) != 0 {
			break
		}
		if currentCtx == nil {
			break
		}
		if currentCtx.IsSet("custom-path") && len(providedCustom) == 0 {
			providedCustom = currentCtx.String("custom-path")
		}
		if currentCtx.IsSet("config") && len(providedConf) == 0 {
			providedConf = currentCtx.String("config")
		}
		if currentCtx.IsSet("work-path") && len(providedWorkPath) == 0 {
			providedWorkPath = currentCtx.String("work-path")
		}
		currentCtx = currentCtx.Parent()
	}
	setting.SetCustomPathAndConf(providedCustom, providedConf, providedWorkPath)

	setAppHelpTemplates()

	if ctx.IsSet("version") {
		cli.ShowVersion(ctx)
		os.Exit(0)
	}

	return nil
}

//设置 AppHelpTemplates
func setAppHelpTemplates() {
	cli.AppHelpTemplate = adjustHelpTemplate(originalAppHelpTemplate)
	cli.CommandHelpTemplate = adjustHelpTemplate(originalCommandHelpTemplate)
	cli.SubcommandHelpTemplate = adjustHelpTemplate(originalSubcommandHelpTemplate)
}

// 打印各命令的 help 提示信息
func adjustHelpTemplate(originalTemplate string) string {
	overrided := ""
	if _, ok := os.LookupEnv("GITEA_CUSTOM"); ok {   // 获取系统环境变量中 'GITEA_CUSTOM' 的值
		overrided = "(GITEA_CUSTOM)"
	}

	return fmt.Sprintf(`%s
DEFAULT CONFIGURATION:
     CustomPath:  %s %s
     CustomConf:  %s
     AppPath:     %s
     AppWorkPath: %s

`, originalTemplate, setting.CustomPath, overrided, setting.CustomConf, setting.AppPath, setting.AppWorkPath)
}

// 获取编译的环境
func formatBuiltWith() string {
	var version = runtime.Version()     // 获取当前 go 语言版本
	if len(MakeVersion) > 0 {
		version = MakeVersion + ", " + runtime.Version()   // Make 编译版本和 go 语言版本
	}
	if len(Tags) == 0 {  // 标签为空
		return " built with " + version
	}

	return " built with " + version + " : " + strings.Replace(Tags, " ", ", ", -1) //Make 版本 + go 版本 + tag标签版本
}
