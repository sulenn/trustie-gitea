// +build windows

// Copyright 2016 The Gitea Authors. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.

package cmd

import (
	"net/http"
)
// windows 系统
//运行 HTTP 服务
func runHTTP(listenAddr string, m http.Handler) error {
	return http.ListenAndServe(listenAddr, m)
}

// 运行 HTTPS 服务
func runHTTPS(listenAddr, certFile, keyFile string, m http.Handler) error {
	return http.ListenAndServeTLS(listenAddr, certFile, keyFile, m)
}
