//start by qiubing
package repo

import (
	"fmt"
	"net/http"

	"code.gitea.io/gitea/models"
	"code.gitea.io/gitea/modules/context"
)

// AddPullRequest adds new pullRequest
func AddPullRequest(ctx *context.Context) {   // 为悬赏 issue 关联合并请求


	pullRequestID := ctx.QueryInt64("newPullRequest")   // 获取传入的合并请求 newPullRequest 的 id 值

	issueIndex := ctx.ParamsInt64("index")       // 该 issue ID 值
	issue, err := models.GetIssueByIndex(ctx.Repo.Repository.ID, issueIndex)   // 该 issue 对象
	if err != nil {
		ctx.ServerError("GetIssueByIndex", err)
		return
	}

	// Redirect
	defer ctx.Redirect(fmt.Sprintf("%s/issues/%d", ctx.Repo.RepoLink, issueIndex), http.StatusSeeOther)  // 重定向到当前页面

	// pullRequest
	pullRequest, err := models.GetIssueByID(pullRequestID)   // pull request 对象
	if err != nil {
		ctx.Flash.Error(ctx.Tr("repo.issues.dependency.add_error_dep_issue_not_exist"))
		return
	}

	// Check if both issues are in the same repo
	if issue.RepoID != pullRequest.RepoID {     // issue 和 pullRequest 是不是同一个 repo
		ctx.Flash.Error(ctx.Tr("repo.issues.dependency.add_error_dep_not_same_repo"))
		return
	}

	// Check if issue and pullRequest is the same
	if pullRequest.Index == issueIndex {   // pullRequest id和issue id是否相等
		ctx.Flash.Error(ctx.Tr("repo.issues.dependency.add_error_same_issue"))
		return
	}

	err = models.UpdateIssuePullRequest(issue, pullRequestID)

	if err != nil {
		ctx.Flash.Error(ctx.Tr("repo.issues.dependency.add_error_cannot_create_circular"))
		return
	}

	ctx.Data["PullRequest"] = pullRequest     // 用于展示

}

// RemovePullRequest removes the pullRequest
func RemovePullRequest(ctx *context.Context) {

	pullRequestID := ctx.QueryInt64("removePullRequestID")

	issueIndex := ctx.ParamsInt64("index")
	issue, err := models.GetIssueByIndex(ctx.Repo.Repository.ID, issueIndex)
	if err != nil {
		ctx.ServerError("GetIssueByIndex", err)
		return
	}

	// Redirect
	ctx.Redirect(fmt.Sprintf("%s/issues/%d", ctx.Repo.RepoLink, issueIndex), http.StatusSeeOther)

	//// Dependency Type
	//depTypeStr := ctx.Req.PostForm.Get("dependencyType")
	//
	//var depType models.DependencyType
	//
	//switch depTypeStr {
	//case "blockedBy":
	//	depType = models.DependencyTypeBlockedBy
	//case "blocking":
	//	depType = models.DependencyTypeBlocking
	//default:
	//	ctx.Error(http.StatusBadRequest, "GetDependecyType")
	//	return
	//}

	// pullRequest
	pullRequest, err := models.GetIssueByID(pullRequestID)
	if err != nil {
		ctx.ServerError("GetIssueByID", err)
		return
	}

	err = models.UpdateIssuePullRequest(issue, 0)   // 用 0 来替换 pullRequestId

	if err != nil {
		ctx.Flash.Error(ctx.Tr("repo.issues.dependency.add_error_cannot_create_circular"))
		return
	}

	//if err = models.RemoveIssueDependency(ctx.User, issue, dep, depType); err != nil {
	//	if models.IsErrDependencyNotExists(err) {
	//		ctx.Flash.Error(ctx.Tr("repo.issues.dependency.add_error_dep_not_exist"))
	//		return
	//	}
	//	ctx.ServerError("RemoveIssueDependency", err)
	//	return
	//}

	ctx.Data["PullRequest"] = pullRequest     // 用于展示

}
//end by 邱冰
