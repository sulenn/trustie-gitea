// Copyright 2016 The Gitea Authors. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.

package routers

import (
	"strings"
	"time"

	"code.gitea.io/gitea/models"
	"code.gitea.io/gitea/models/migrations"
	"code.gitea.io/gitea/modules/cache"
	"code.gitea.io/gitea/modules/cron"
	"code.gitea.io/gitea/modules/git"
	"code.gitea.io/gitea/modules/highlight"
	issue_indexer "code.gitea.io/gitea/modules/indexer/issues"
	"code.gitea.io/gitea/modules/log"
	"code.gitea.io/gitea/modules/markup"
	"code.gitea.io/gitea/modules/markup/external"
	"code.gitea.io/gitea/modules/setting"
	"code.gitea.io/gitea/modules/ssh"
	"code.gitea.io/gitea/modules/task"
	"code.gitea.io/gitea/services/mailer"
	mirror_service "code.gitea.io/gitea/services/mirror"

	"gitea.com/macaron/macaron"
)

//检查运行模型, 如 prod
func checkRunMode() {
	switch setting.Cfg.Section("").Key("RUN_MODE").String() {
	case "prod":
		macaron.Env = macaron.PROD
		macaron.ColorLog = false
		setting.ProdMode = true
	default:
		git.Debug = true
	}
	log.Info("Run Mode: %s", strings.Title(macaron.Env))
}

// NewServices init new services
//初始化各种服务配置, 如 据库\日志\session\邮件队列\cache 服务 等
func NewServices() {
	setting.NewServices()  // 初始化各种服务配置,如 数据库\日志\session 等
	mailer.NewContext()  //启动邮件队列服务
	_ = cache.NewContext()  //开始 cache 缓存服务
}

// In case of problems connecting to DB, retry connection. Eg, PGSQL in Docker Container on Synology
func initDBEngine() (err error) {
	log.Info("Beginning ORM engine initialization.")
	for i := 0; i < setting.Database.DBConnectRetries; i++ {
		log.Info("ORM engine initialization attempt #%d/%d...", i+1, setting.Database.DBConnectRetries)
		if err = models.NewEngine(migrations.Migrate); err == nil {
			break
		} else if i == setting.Database.DBConnectRetries-1 {
			return err
		}
		log.Error("ORM engine initialization attempt #%d/%d failed. Error: %v", i+1, setting.Database.DBConnectRetries, err)
		log.Info("Backing off for %d seconds", int64(setting.Database.DBConnectBackoff/time.Second))
		time.Sleep(setting.Database.DBConnectBackoff)
	}
	models.HasEngine = true
	return nil
}

// GlobalInit is for global configuration reload-able.
//初始化各项全局配置信息, 开启各种全局服务
func GlobalInit() {
	setting.NewContext()    // 初始化配置上下文信息,加载 custom/conf/app.ini 中的配置信息
	if err := git.Init(); err != nil {   // 初始化 git 模块的信息
		log.Fatal("Git module init failed: %v", err)
	}
	setting.CheckLFSVersion()   //检查 LFS 版本, 条件为 git 版本大于等于 2.1.2 ,不满足条件就关掉
	log.Trace("AppPath: %s", setting.AppPath)
	log.Trace("AppWorkPath: %s", setting.AppWorkPath)
	log.Trace("Custom path: %s", setting.CustomPath)
	log.Trace("Log path: %s", setting.LogRootPath)

	NewServices()   //初始化各种服务配置, 如 据库\日志\session\邮件队列\cache 服务 等

	if setting.InstallLock {
		highlight.NewContext()  //初始化 highlight.mapping 相关配置内容
		external.RegisterParsers()  // 根据设置注册所有支持的第三方解析器
		markup.Init()  //为 markdown 解析初始化正则表达式
		if err := initDBEngine(); err == nil {   // 初始化 ORM 数据库引擎
			log.Info("ORM engine initialization successful!")
		} else {
			log.Fatal("ORM engine initialization failed: %v", err)
		}

		if err := models.InitOAuth2(); err != nil {   // 初始化 Oauth, 允许用户授权第三方移动应用
			log.Fatal("Failed to initialize OAuth2 support: %v", err)
		}

		models.NewRepoContext()   //初始化 reposiroty 上下文信息, 如加载 "gitignore", "license", "readme", "label" 等

		// Booting long running goroutines.
		cron.NewContext()  //cron:Command Run On, 指挥调度运行.创建各种任务调度.如 mirrorUpdate\gitFsck\checkRepos\archiveCleanup 等
		if err := issue_indexer.InitIssueIndexer(false); err != nil {   // //初始化 issue 索引器
			log.Fatal("Failed to initialize issue indexer: %v", err)
		}
		models.InitRepoIndexer()   //初始化 repo 索引器
		mirror_service.InitSyncMirrors()  // 初始化 go 线程同步仓库镜像
		models.InitDeliverHooks()  //启动钩子分发线程
		models.InitTestPullRequests()  //运行线程,检查所有 pull request 状态
		if err := task.Init(); err != nil {   // //启动服务, 获取所有尚未结束的任务,然后运行这些任务
			log.Fatal("Failed to initialize task scheduler: %v", err)
		}
	}
	if setting.EnableSQLite3 {
		log.Info("SQLite3 Supported")
	}
	checkRunMode()   // //检查运行模型, 如 prod

	if setting.InstallLock && setting.SSH.StartBuiltinServer {
		ssh.Listen(setting.SSH.ListenHost, setting.SSH.ListenPort, setting.SSH.ServerCiphers, setting.SSH.ServerKeyExchanges, setting.SSH.ServerMACs)
		log.Info("SSH server started on %s:%d. Cipher list (%v), key exchange algorithms (%v), MACs (%v)", setting.SSH.ListenHost, setting.SSH.ListenPort, setting.SSH.ServerCiphers, setting.SSH.ServerKeyExchanges, setting.SSH.ServerMACs)
	}
}
