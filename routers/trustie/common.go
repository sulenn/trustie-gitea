package trustie

//http 请求返回的公共信息模板
type CommonResponse struct {
	Status int
	Description string
}

//返回 GitUrl 项目远程版本仓库地址
type GitUrlResponse struct {
	Status int
	GitUrl string
}

//返回新增、删除的行数
type LinesResponse struct {
	Status int
	Modifications int
}
