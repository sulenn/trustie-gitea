package trustie

import (
	"code.gitea.io/gitea/models"
	"code.gitea.io/gitea/modules/auth"
	"code.gitea.io/gitea/modules/base"
	"code.gitea.io/gitea/modules/context"
	api "code.gitea.io/gitea/modules/structs"
	"code.gitea.io/gitea/modules/util"
	"code.gitea.io/gitea/routers/api/v1/convert"
	"fmt"
	//"net/http"
	"reflect"
	"strings"
)

const (
	// tplMustChangePassword template for updating a user's password
	tplMustChangePassword = "user/auth/change_passwd"
	// tplSignIn template for sign in page
	tplSignIn base.TplName = "user/auth/signin"
	// tplSignUp template path for sign up page
	tplSignUp base.TplName = "user/auth/signup"
	// TplActivate template path for activate user
	TplActivate       base.TplName = "user/auth/activate"
	tplForgotPassword base.TplName = "user/auth/forgot_passwd"
	tplResetPassword  base.TplName = "user/auth/reset_passwd"
	tplTwofa          base.TplName = "user/auth/twofa"
	tplTwofaScratch   base.TplName = "user/auth/twofa_scratch"
	tplLinkAccount    base.TplName = "user/auth/link_account"
	tplU2F            base.TplName = "user/auth/u2f"
)

//判断用户该项目名是否已被使用
func JudgeReponame(ctx *context.Context, form auth.UserAndRepo) {
	ctx.Data["Title"] = ctx.Tr("sign_in")

	u, er := models.UserSignIn(form.UserName, form.Password)    // 判断用户是否存在， 参考用户首页登录 /routers/user/auth.go 中 SignInPost 方法
	if er != nil {
		ctx.JSON(200, CommonResponse{500, "该用户不存在"})
		return
	}
	fmt.Println(reflect.TypeOf(u))

	opts := &models.SearchRepoOptions{           // 获取用户已创建的项目，参考用户登录后的首页显示 /routers/api/v1/repo/repo.go 中 Search 方法
		Keyword:            strings.Trim(ctx.Query("q"), " "),
		OwnerID:            ctx.QueryInt64("uid"),
		Page:               ctx.QueryInt("page"),
		PageSize:           convert.ToCorrectPageSize(ctx.QueryInt("limit")),
		TopicOnly:          ctx.QueryBool("topic"),
		Collaborate:        util.OptionalBoolNone,
		Private:            ctx.IsSigned && (ctx.Query("private") == "" || ctx.QueryBool("private")),
		UserIsAdmin:        ctx.IsUserSiteAdmin(),
		UserID:             ctx.Data["SignedUserID"].(int64),
		StarredByID:        ctx.QueryInt64("starredBy"),
		IncludeDescription: ctx.QueryBool("includeDesc"),
	}

	if ctx.QueryBool("exclusive") {
		opts.Collaborate = util.OptionalBoolFalse
	}

	var mode = ctx.Query("mode")
	switch mode {
	case "source":
		opts.Fork = util.OptionalBoolFalse
		opts.Mirror = util.OptionalBoolFalse
	case "fork":
		opts.Fork = util.OptionalBoolTrue
	case "mirror":
		opts.Mirror = util.OptionalBoolTrue
	case "collaborative":
		opts.Mirror = util.OptionalBoolFalse
		opts.Collaborate = util.OptionalBoolTrue
	case "":
	default:
		//ctx.Error(http.StatusUnprocessableEntity, "", fmt.Errorf("Invalid search mode: \"%s\"", mode))
		ctx.JSON(200, CommonResponse{500, ""})
		return
	}

	var sortMode = ctx.Query("sort")
	if len(sortMode) > 0 {
		var sortOrder = ctx.Query("order")
		if len(sortOrder) == 0 {
			sortOrder = "asc"
		}
		if searchModeMap, ok := searchOrderByMap[sortOrder]; ok {
			if orderBy, ok := searchModeMap[sortMode]; ok {
				opts.OrderBy = orderBy
			} else {
				//ctx.Error(http.StatusUnprocessableEntity, "", fmt.Errorf("Invalid sort mode: \"%s\"", sortMode))
				ctx.JSON(200, CommonResponse{500, ""})
				return
			}
		} else {
			//ctx.Error(http.StatusUnprocessableEntity, "", fmt.Errorf("Invalid sort order: \"%s\"", sortOrder))
			ctx.JSON(200, CommonResponse{500, ""})
			return
		}
	}

	var err error
	repos, _, err := models.SearchRepository(opts)
	if err != nil {
		//ctx.JSON(500, api.SearchError{
		//	OK:    false,
		//	Error: err.Error(),
		//})
		ctx.JSON(200, CommonResponse{500, ""})
		return
	}

	results := make([]*api.Repository, len(repos))   // 获取所有的 repos
	for i, repo := range repos {
		if err = repo.GetOwner(); err != nil {
			ctx.JSON(200, CommonResponse{500, ""})
			return
		}
		accessMode, err := models.AccessLevel(ctx.User, repo)
		if err != nil {
			ctx.JSON(200, CommonResponse{500, ""})
			return
		}
		results[i] = repo.APIFormat(accessMode)
	}

	for _, repo := range results {     // 判断该用户项目名是否被使用
		if repo.Name == form.Reponame {
			ctx.JSON(200, CommonResponse{500, ""})
			return
		}
	}
	ctx.JSON(200, &CommonResponse{200, ""})
	return
}
