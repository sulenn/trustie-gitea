package trustie

import (
	"code.gitea.io/gitea/models"
	"code.gitea.io/gitea/modules/auth"
	"code.gitea.io/gitea/modules/context"
	"code.gitea.io/gitea/modules/git"
	"code.gitea.io/gitea/modules/setting"
	"code.gitea.io/gitea/services/gitdiff"
)

// 判断提交的 commit hash 值是否存在，存在则返回修改的行数
func JudgeCommitHashInfo(ctx *context.Context, form auth.CommitHashInfo) {
	ctx.Data["PageIsDiff"] = true
	ctx.Data["RequireHighlightJS"] = true

	userName := form.Username
	repoName := form.Reponame

	gitRepo, err := git.OpenRepository(models.RepoPath(userName, repoName))   // 获取 gitRepo，通过用户登录后首页获得来灵感 /modules/context/repo.go 第423行，拼凑获得 gitRepo 条件
	if err != nil {
		ctx.JSON(200, CommonResponse{500, ""})
		return
	}
	ctx.Repo.GitRepo = gitRepo

	modification := 0       // 新增、删除行数
	for _, commitID := range form.ShaList {

		commit, err := ctx.Repo.GitRepo.GetCommit(commitID)       // 获得 commit，需要通过 gitRepo，参考用户某个项目的某次 commit 提交 /routers/repo/commit 193 行 Diff() 函数
		if err != nil {
			if git.IsErrNotExist(err) {
				ctx.JSON(200, CommonResponse{500, "commit hash 值不存在"})
			} else {
				ctx.JSON(200, CommonResponse{500, "commit hash 值不存在"})
			}
			return
		}
		if len(commitID) != 40 {
			commitID = commit.ID.String()
		}

		diff, err := gitdiff.GetDiffCommit(models.RepoPath(userName, repoName),     // 获取某一次 commit hash 和前一次 commit hash 的区别
			commitID, setting.Git.MaxGitDiffLines,
			setting.Git.MaxGitDiffLineCharacters, setting.Git.MaxGitDiffFiles)
		if err != nil {
			ctx.JSON(200, CommonResponse{500, ""})
			return
		}


		//for _, file := range diff.Files {
		//	for _, line := range file.Sections[0].Lines {
		//		if line.Content[0] == '+' || line.Content[0] == '-' {
		//			modification++
		//		}
		//	}
		//}
		modification = modification + diff.TotalAddition + diff.TotalDeletion

	}

	ctx.JSON(200, &LinesResponse{200, modification})
	return
}
