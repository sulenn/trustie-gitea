package trustie

import (
	"code.gitea.io/gitea/models"
	"code.gitea.io/gitea/modules/auth"
	"code.gitea.io/gitea/modules/context"
	"code.gitea.io/gitea/modules/log"
	"code.gitea.io/gitea/modules/notification"
	"code.gitea.io/gitea/modules/setting"
	api "code.gitea.io/gitea/modules/structs"
	"fmt"
	//"net/http"
)

var searchOrderByMap = map[string]map[string]models.SearchOrderBy{
	"asc": {
		"alpha":   models.SearchOrderByAlphabetically,
		"created": models.SearchOrderByOldest,
		"updated": models.SearchOrderByLeastUpdated,
		"size":    models.SearchOrderBySize,
		"id":      models.SearchOrderByID,
	},
	"desc": {
		"alpha":   models.SearchOrderByAlphabeticallyReverse,
		"created": models.SearchOrderByNewest,
		"updated": models.SearchOrderByRecentUpdated,
		"size":    models.SearchOrderBySizeReverse,
		"id":      models.SearchOrderByIDReverse,
	},
}


// 创建远程版本仓库，并返回远程版本仓库的 URL 地址
func CreateRemoteRepo(ctx *context.Context, form auth.UserAndRepo) {
	u, er := models.UserSignIn(form.UserName, form.Password)     // 判断用户是否存在， 参考用户首页登录 /routers/user/auth.go 中 SignInPost 方法
	if er != nil {
		ctx.JSON(200, CommonResponse{500, "该用户不存在"})
		return
	}

	ctx.Data["Title"] = ctx.Tr("new_repo")               // 用户创建远程版本仓库，参考用户创建项目的页面 /routers/repo/repo.go 中 CreatePost 方法

	ctx.Data["Gitignores"] = models.Gitignores
	ctx.Data["LabelTemplates"] = models.LabelTemplates
	ctx.Data["Licenses"] = models.Licenses
	ctx.Data["Readmes"] = models.Readmes
	ctx.User = u
	ctxUser := checkContextUser(ctx, u.ID)
	if ctx.Written() {
		return
	}
	ctx.Data["ContextUser"] = ctxUser

	if ctx.HasError() {
		ctx.JSON(200, CommonResponse{500, ""})
		return
	}

	repo, err := models.CreateRepository(ctx.User, ctxUser, models.CreateRepoOptions{        // 创建项目远程版本仓库
		Name:        form.Reponame,
		Description: "",
		Gitignores:  "",
		IssueLabels: "",
		License:     "",
		Readme:      "Default",
		IsPrivate:   false || setting.Repository.ForcePrivate,
		AutoInit:    false,
	})
	if err == nil {
		notification.NotifyCreateRepository(ctx.User, ctxUser, repo)

		var result *api.Repository          // 获取新创建的项目的 HTMLURL，参考用户登录后的首页显示 /routers/api/v1/repo/repo.go 中 Search 方法
		if err = repo.GetOwner(); err != nil {
			ctx.JSON(200, CommonResponse{500, ""})
			return
		}
		accessMode, err := models.AccessLevel(ctx.User, repo)
		if err != nil {
			ctx.JSON(200, CommonResponse{500, ""})
			return
		}
		result = repo.APIFormat(accessMode)
		//fmt.Println(reflect.TypeOf(result))
		url := result.HTMLURL    // 远程版本仓库的 url

		ctx.JSON(200, &GitUrlResponse{200, url})
		return
	}

	if repo != nil {
		if errDelete := models.DeleteRepository(ctx.User, ctxUser.ID, repo.ID); errDelete != nil {
			log.Error("DeleteRepository: %v", errDelete)
		}
	}

	ctx.JSON(200, CommonResponse{500, ""})

}

func checkContextUser(ctx *context.Context, uid int64) *models.User {
	orgs, err := models.GetOwnedOrgsByUserIDDesc(ctx.User.ID, "updated_unix")
	if err != nil {
		ctx.ServerError("GetOwnedOrgsByUserIDDesc", err)
		return nil
	}
	ctx.Data["Orgs"] = orgs

	// Not equal means current user is an organization.
	if uid == ctx.User.ID || uid == 0 {
		return ctx.User
	}

	org, err := models.GetUserByID(uid)
	if models.IsErrUserNotExist(err) {
		return ctx.User
	}

	if err != nil {
		ctx.ServerError("GetUserByID", fmt.Errorf("[%d]: %v", uid, err))
		return nil
	}

	// Check ownership of organization.
	if !org.IsOrganization() {
		ctx.Error(403)
		return nil
	}
	if !ctx.User.IsAdmin {
		isOwner, err := org.IsOwnedBy(ctx.User.ID)
		if err != nil {
			ctx.ServerError("IsOwnedBy", err)
			return nil
		} else if !isOwner {
			ctx.Error(403)
			return nil
		}
	}
	return org
}
