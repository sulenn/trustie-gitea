// Copyright 2016 The Gitea Authors. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.

package options

//go:generate go run -mod=vendor main.go

type directorySet map[string][]string // 用于存储全局的一些字符串数据，例如 locale:{locale_es-ES.ini, locale_nb-NO.ini, locale_zh-CN.ini.........}本地化配置文件。

func (s directorySet) Add(key string, value []string) {   // 往 directorySet 中添加新的字符串数据
	_, ok := s[key]

	if !ok {
		s[key] = make([]string, 0, len(value))
	}

	s[key] = append(s[key], value...)
}

func (s directorySet) Get(key string) []string {  // 从 directorySet 中获取某个字符串 key 的数据
	_, ok := s[key]

	if ok {
		result := []string{}
		seen := map[string]string{}

		for _, val := range s[key] {     // 硬拷贝
			if _, ok := seen[val]; !ok {   // 去除重复数据
				result = append(result, val)
				seen[val] = val
			}
		}

		return result
	}

	return []string{}
}

// 先将 key、value 压入 directorySet 中，然后再从 directorySet 中将 key 去除重复元素的拷贝出来
func (s directorySet) AddAndGet(key string, value []string) []string {
	s.Add(key, value)
	return s.Get(key)
}

//判断文件集合 s 中是否存在 文件 key
func (s directorySet) Filled(key string) bool {
	return len(s[key]) > 0
}
