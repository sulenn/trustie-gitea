// +build !bindata

// Copyright 2016 The Gitea Authors. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.

package options

import (
	"fmt"
	"io/ioutil"
	"path"

	"code.gitea.io/gitea/modules/setting"

	"github.com/unknwon/com"
)

var (
	directories = make(directorySet)   // 用于存储全局的一些字符串数据，例如 locale:{locale_es-ES.ini, locale_nb-NO.ini, locale_zh-CN.ini.........}本地化配置文件。
)

// Dir returns all files from static or custom directory.
//根据 name 拼接 static 或 custom 路径,然后从路径中获取所有文件
func Dir(name string) ([]string, error) {
	if directories.Filled(name) {  //判断文件集合 s 中是否存在文件 name
		return directories.Get(name), nil
	}

	var (
		result []string
	)

	customDir := path.Join(setting.CustomPath, "options", name)  // 拼接定制化路径，指向主目录 /custom. 如 /home/qiubing/GOPATH/src/code.gitea.io/gitea/custom/options/gitignore

	if com.IsDir(customDir) {  // 判断目录是否存在
		files, err := com.StatDir(customDir, true)

		if err != nil {
			return []string{}, fmt.Errorf("Failed to read custom directory. %v", err)
		}

		result = append(result, files...)
	}

	staticDir := path.Join(setting.StaticRootPath, "options", name)   // 拼接静态路径，指向主目录 /options, 如 /home/qiubing/GOPATH/src/code.gitea.io/gitea/options/gitignore

	if com.IsDir(staticDir) {
		files, err := com.StatDir(staticDir, true)  // 返回路径中所有的文件，包括子目录中的文件

		if err != nil {
			return []string{}, fmt.Errorf("Failed to read static directory. %v", err)
		}

		result = append(result, files...)
	}

	return directories.AddAndGet(name, result), nil      // 先将 key、value 压入 directorySet 中，然后再从 directorySet 中将 key 去除重复元素的拷贝出来
}

// Locale reads the content of a specific locale from static or custom path.
func Locale(name string) ([]byte, error) {
	return fileFromDir(path.Join("locale", name))   // path.Join 用于拼接目录和文件， fileFromDir 用于从文件中读取内容
}

// Readme reads the content of a specific readme from static or custom path.
func Readme(name string) ([]byte, error) {
	return fileFromDir(path.Join("readme", name))
}

// Gitignore reads the content of a specific gitignore from static or custom path.
func Gitignore(name string) ([]byte, error) {
	return fileFromDir(path.Join("gitignore", name))
}

// License reads the content of a specific license from static or custom path.
func License(name string) ([]byte, error) {
	return fileFromDir(path.Join("license", name))
}

// Labels reads the content of a specific labels from static or custom path.
func Labels(name string) ([]byte, error) {
	return fileFromDir(path.Join("label", name))
}

// fileFromDir is a helper to read files from static or custom path.
func fileFromDir(name string) ([]byte, error) {
	customPath := path.Join(setting.CustomPath, "options", name)  // 拼接定制化路径，指向主目录/custom/options/locale

	if com.IsFile(customPath) {  // 文件是否存在
		return ioutil.ReadFile(customPath)   // 读取文件中的内容
	}

	staticPath := path.Join(setting.StaticRootPath, "options", name)   // 拼接静态路径，指向主目录 /options/locale

	if com.IsFile(staticPath) {
		return ioutil.ReadFile(staticPath)
	}

	return []byte{}, fmt.Errorf("Asset file does not exist: %s", name)
}
