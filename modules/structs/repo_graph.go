package structs

// add by qiubing

type Graph struct {
	GraphAcii    string `json:"graph_acii"`
	Relation     string `json:"relation"`
	Branch       string `json:"branch"`
	Rev          string `json:"sha"`
	Date         string `json:"date"`
	Author       string `json:"author"`
	AuthorEmail  string `json:"author_email"`
	ShortRev     string `json:"short_sha"`
	Subject      string `json:"message"`
	OnlyRelation bool   `json:"only_relation"`
}
