// Copyright 2014 The Gogs Authors. All rights reserved.
// Copyright 2017 The Gitea Authors. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.

package setting

import (
	"encoding/base64"
	"fmt"
	"io"
	"io/ioutil"
	"net"
	"net/url"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"runtime"
	"strconv"
	"strings"
	"time"

	"code.gitea.io/gitea/modules/generate"
	"code.gitea.io/gitea/modules/git"
	"code.gitea.io/gitea/modules/log"
	_ "code.gitea.io/gitea/modules/minwinsvc" // import minwinsvc for windows services
	"code.gitea.io/gitea/modules/user"

	shellquote "github.com/kballard/go-shellquote"
	version "github.com/mcuadros/go-version"
	"github.com/unknwon/cae/zip"
	"github.com/unknwon/com"
	ini "gopkg.in/ini.v1"
	"strk.kbt.io/projects/go/libravatar"
)

// Scheme describes protocol types
type Scheme string

// enumerates all the scheme types
const (
	HTTP       Scheme = "http"
	HTTPS      Scheme = "https"
	FCGI       Scheme = "fcgi"
	UnixSocket Scheme = "unix"
)

// LandingPage describes the default page
type LandingPage string

// enumerates all the landing page types
const (
	LandingPageHome          LandingPage = "/"
	LandingPageExplore       LandingPage = "/explore"
	LandingPageOrganizations LandingPage = "/explore/organizations"
)

// enumerates all the types of captchas   // 枚举所有验证码类型
const (
	ImageCaptcha = "image"   //图片验证码
	ReCaptcha    = "recaptcha"    //google 公司的验证码服务,只需要点一下复选框，Google会收集一些鼠标轨迹、网络信息、浏览器信息等等，依靠后端的神经网络判断是机器还是人，绝大多数验证会一键通过，无需像传统验证码一样。
)

// settings
var (
	// AppVer settings
	AppVer         string     // 当前应用（Gitea）版本，main.go 48
	AppBuiltWith   string     // 当前 go 语言版本，main.go 中 49
	AppName        string     // 应用名 初始化./setting.go 542. setting.go 546 会从app.ini读数据覆盖
	AppURL         string     // 应用的 url,如 http://localhost:3000/
	AppSubURL      string       // 应用子 url, url 中去除 'http://localhost:3000/' 的路径
	AppSubURLDepth int // Number of slashes   应用子路径的深度,例如 'sulenn/nudt27" 的深度为2
	AppPath        string    // app 应用的绝对路径.如 /home/qiubing/GOPATH/src/code.gitea.io/gitea/gitea
	AppDataPath    string    // app 应用的数据存放路径, 如 /home/qiubing/GOPATH/src/code.gitea.io/gitea/data
	AppWorkPath    string    // app 应用的工作路径. 如 /home/qiubing/GOPATH/src/code.gitea.io/gitea

	// Server settings
	Protocol             Scheme   //应用层通信协议,http\https等
	Domain               string   //域名
	HTTPAddr             string   //http 地址,如0.0.0.0
	HTTPPort             string   //http 端口,如3000
	LocalURL             string   //本地 url,如 http://localhost:--help/
	RedirectOtherPort    bool     // 是否重定向其它端口,如 false
	PortToRedirect       string   //如 80
	OfflineMode          bool     // 如 false
	CertFile             string
	KeyFile              string
	StaticRootPath       string   // 如 /home/qiubing/GOPATH/src/code.gitea.io/gitea
	EnableGzip           bool     // 如 false
	LandingPageURL       LandingPage   // 主页 url, 如 "/"
	UnixSocketPermission uint32
	EnablePprof          bool    // 如 false
	PprofDataPath        string  // 如 /home/qiubing/GOPATH/src/code.gitea.io/gitea/data/tmp/pprof
	EnableLetsEncrypt    bool
	LetsEncryptTOS       bool
	LetsEncryptDirectory string
	LetsEncryptEmail     string

	SSH = struct {
		Disabled                 bool           `ini:"DISABLE_SSH"`
		StartBuiltinServer       bool           `ini:"START_SSH_SERVER"`
		BuiltinServerUser        string         `ini:"BUILTIN_SSH_SERVER_USER"`   // 编译服务的用户, 如 qiubing
		Domain                   string         `ini:"SSH_DOMAIN"`   // ssh 域名,如 localhost
		Port                     int            `ini:"SSH_PORT"`     // 端口, 如 22
		ListenHost               string         `ini:"SSH_LISTEN_HOST"`
		ListenPort               int            `ini:"SSH_LISTEN_PORT"`  // 监听端口, 如 22
		RootPath                 string         `ini:"SSH_ROOT_PATH"`   // 根路径, 如 /home/qiubing/.ssh
		ServerCiphers            []string       `ini:"SSH_SERVER_CIPHERS"`
		ServerKeyExchanges       []string       `ini:"SSH_SERVER_KEY_EXCHANGES"`
		ServerMACs               []string       `ini:"SSH_SERVER_MACS"`
		KeyTestPath              string         `ini:"SSH_KEY_TEST_PATH"`  // 如 /tmp
		KeygenPath               string         `ini:"SSH_KEYGEN_PATH"`    // 如 ssh-keygen
		AuthorizedKeysBackup     bool           `ini:"SSH_AUTHORIZED_KEYS_BACKUP"`  // 如 true
		MinimumKeySizeCheck      bool           `ini:"-"`  // 如 false
		MinimumKeySizes          map[string]int `ini:"-"`  // 如 空的 map[string]int
		CreateAuthorizedKeysFile bool           `ini:"SSH_CREATE_AUTHORIZED_KEYS_FILE"`  // 如 true
		ExposeAnonymous          bool           `ini:"SSH_EXPOSE_ANONYMOUS"`   // 如 false
	}{
		Disabled:           false,
		StartBuiltinServer: false,
		Domain:             "",
		Port:               22,
		ServerCiphers:      []string{"aes128-ctr", "aes192-ctr", "aes256-ctr", "aes128-gcm@openssh.com", "arcfour256", "arcfour128"},
		ServerKeyExchanges: []string{"diffie-hellman-group1-sha1", "diffie-hellman-group14-sha1", "ecdh-sha2-nistp256", "ecdh-sha2-nistp384", "ecdh-sha2-nistp521", "curve25519-sha256@libssh.org"},
		ServerMACs:         []string{"hmac-sha2-256-etm@openssh.com", "hmac-sha2-256", "hmac-sha1", "hmac-sha1-96"},
		KeygenPath:         "ssh-keygen",
	}

	LFS struct {
		StartServer     bool          `ini:"LFS_START_SERVER"`  // 如 true
		ContentPath     string        `ini:"LFS_CONTENT_PATH"`  // 内容路径, 如 /home/qiubing/GOPATH/src/code.gitea.io/gitea/data/lfs
		JWTSecretBase64 string        `ini:"LFS_JWT_SECRET"`   // 如 V6JYb6hc3rNL9JTW00OekXQTu1a1Q3NejtO1yEPykik
		JWTSecretBytes  []byte        `ini:"-"`
		HTTPAuthExpiry  time.Duration `ini:"LFS_HTTP_AUTH_EXPIRY"`   // 如 HTTPAuthExpiry
	}

	// Security settings
	InstallLock           bool  // 如 true
	SecretKey             string  // 如 PAjy9jNvp45h9XjeXgmofwrXyJX2ICmayuukU1jBWTMA7k2Mqge2yxtExlp0YJDb
	LogInRememberDays     int  // 如 7
	CookieUserName        string  // 如 gitea_awesome
	CookieRememberName    string  // 如 gitea_incredible
	ReverseProxyAuthUser  string  // 如 X-WEBAUTH-USER
	ReverseProxyAuthEmail string  // 如 X-WEBAUTH-EMAIL
	MinPasswordLength     int  // 最小密码长度, 如 6
	ImportLocalPaths      bool  // 如 false
	DisableGitHooks       bool  // 如 false
	PasswordComplexity    []string  // 如:空的 []string
	PasswordHashAlgo      string  // 如 pbkdf2

	// UI settings
	UI = struct {
		ExplorePagingNum      int  // 搜索功能中每页可展示的项目数量,如搜索项目时每页展示 20
		IssuePagingNum        int
		RepoSearchPagingNum   int
		FeedMaxCommitNum      int
		GraphMaxCommitNum     int
		CodeCommentLines      int
		ReactionMaxUserNum    int
		ThemeColorMetaTag     string
		MaxDisplayFileSize    int64
		ShowUserEmail         bool   // 显示用户邮箱, 如 true
		DefaultShowFullName   bool   // 如 false
		DefaultTheme          string
		Themes                []string
		SearchRepoDescription bool  // 如 true

		Admin struct {
			UserPagingNum   int
			RepoPagingNum   int   // 管理员可查看的项目数量
			NoticePagingNum int
			OrgPagingNum    int
		} `ini:"ui.admin"`
		User struct {
			RepoPagingNum int
		} `ini:"ui.user"`
		Meta struct {
			Author      string
			Description string
			Keywords    string
		} `ini:"ui.meta"`
	}{
		ExplorePagingNum:    20,
		IssuePagingNum:      10,
		RepoSearchPagingNum: 10,
		FeedMaxCommitNum:    5,
		GraphMaxCommitNum:   100,
		CodeCommentLines:    4,
		ReactionMaxUserNum:  10,
		ThemeColorMetaTag:   `#6cc644`,
		MaxDisplayFileSize:  8388608,
		DefaultTheme:        `gitea`,
		Themes:              []string{`gitea`, `arc-green`},
		Admin: struct {
			UserPagingNum   int
			RepoPagingNum   int
			NoticePagingNum int
			OrgPagingNum    int
		}{
			UserPagingNum:   50,
			RepoPagingNum:   50,
			NoticePagingNum: 25,
			OrgPagingNum:    50,
		},
		User: struct {
			RepoPagingNum int
		}{
			RepoPagingNum: 15,
		},
		Meta: struct {
			Author      string
			Description string
			Keywords    string
		}{
			Author:      "Gitea - Git with a cup of tea",
			Description: "Gitea (Git with a cup of tea) is a painless self-hosted Git service written in Go",
			Keywords:    "go,git,self-hosted,gitea",
		},
	}

	// Markdown settings
	Markdown = struct {
		EnableHardLineBreak bool
		CustomURLSchemes    []string `ini:"CUSTOM_URL_SCHEMES"`
		FileExtensions      []string
	}{
		EnableHardLineBreak: false,
		FileExtensions:      strings.Split(".md,.markdown,.mdown,.mkd", ","),
	}

	// Admin settings
	Admin struct {
		DisableRegularOrgCreation bool
		DefaultEmailNotification  string      // 默认邮件通知 enabled
	}

	// Picture settings
	AvatarUploadPath              string  // 头像上传的路径, 如 /home/qiubing/GOPATH/src/code.gitea.io/gitea/data/avatars
	AvatarMaxWidth                int  // 头像最大宽 4096
	AvatarMaxHeight               int  // 头像最大高 3072
	GravatarSource                string  // 头像源 https://secure.gravatar.com/avatar/
	GravatarSourceURL             *url.URL  // 对 GravatarSource 的解析
	DisableGravatar               bool  // 如 false
	EnableFederatedAvatar         bool  //如 true
	LibravatarService             *libravatar.Libravatar  // 第三方的 gavatar 服务
	AvatarMaxFileSize             int64  // 头像最大文件大小 1048576
	RepositoryAvatarUploadPath    string  // 仓库头像上传路径, 如 /home/qiubing/GOPATH/src/code.gitea.io/gitea/data/repo-avatars
	RepositoryAvatarFallback      string  // 如 none
	RepositoryAvatarFallbackImage string  // 如 /img/repo_default.png

	// Log settings
	LogLevel           string    // 日志级别 level
	StacktraceLogLevel string    // 栈追踪日志级别?
	LogRootPath        string    // 日志存放根路径
	LogDescriptions    = make(map[string]*LogDescription)  // 日志描述,modules/setting/log.go 中 278
	RedirectMacaronLog bool      // 重定向的 macalon 日志
	DisableRouterLog   bool      // 如,false
	RouterLogLevel     log.Level    // 路由日志的级别 level
	RouterLogMode      string
	EnableAccessLog    bool
	AccessLogTemplate  string
	EnableXORMLog      bool

	// Attachment settings
	AttachmentPath         string     // 如 /home/qiubing/GOPATH/src/code.gitea.io/gitea/data/attachments
	AttachmentAllowedTypes string  // 如 image/jpeg,image/png,application/zip,application/gzip
	AttachmentMaxSize      int64  // 如 4
	AttachmentMaxFiles     int  // 如 5
	AttachmentEnabled      bool  // 如 true

	// Time settings
	TimeFormat string
	// UILocation is the location on the UI, so that we can display the time on UI.
	DefaultUILocation = time.Local

	CSRFCookieName     = "_csrf"
	CSRFCookieHTTPOnly = true  // 如 true

	// Mirror settings
	Mirror struct {
		DefaultInterval time.Duration  //默认间隔时间 28800000000000
		MinInterval     time.Duration  // 最小间隔时间 xorm.io/core.CacheGcInterval
	}

	// API settings
	API = struct {
		EnableSwagger          bool
		SwaggerURL             string   // 如 http://localhost:3000/api/swagger
		MaxResponseItems       int
		DefaultPagingNum       int
		DefaultGitTreesPerPage int
		DefaultMaxBlobSize     int64
	}{  // API 初始化值
		EnableSwagger:          true,
		SwaggerURL:             "",
		MaxResponseItems:       50,
		DefaultPagingNum:       30,
		DefaultGitTreesPerPage: 1000,
		DefaultMaxBlobSize:     10485760,
	}

	OAuth2 = struct {
		Enable                     bool
		AccessTokenExpirationTime  int64
		RefreshTokenExpirationTime int64
		InvalidateRefreshTokens    bool
		JWTSecretBytes             []byte `ini:"-"`
		JWTSecretBase64            string `ini:"JWT_SECRET"`   // 如 HK_fKyaB2ILdEoKuO_PhK0PFfQWdCCk0R4i3d3rOwOw
	}{
		Enable:                     true,
		AccessTokenExpirationTime:  3600,
		RefreshTokenExpirationTime: 730,
		InvalidateRefreshTokens:    false,
	}

	U2F = struct {
		AppID         string
		TrustedFacets []string
	}{}

	// Metrics settings
	Metrics = struct {
		Enabled bool
		Token   string
	}{
		Enabled: false,
		Token:   "",
	}

	// I18n settings
	Langs     []string  // 22 种本地化语言的英文编号名称
	Names     []string  // 22 种本地化语言的中文名称
	dateLangs map[string]string // 如:空的 map[string]string

	// Highlight settings are loaded in modules/template/highlight.go

	// Other settings
	ShowFooterBranding         bool  // 如 false
	ShowFooterVersion          bool  // 如 true
	ShowFooterTemplateLoadTime bool  // 如 true

	// Global setting objects
	Cfg           *ini.File   // 加载 custom/conf/app.ini 类型的配置文件
	CustomPath    string // Custom directory path 定制化文件夹路径 setting.go 497. /home/qiubing/GOPATH/src/code.gitea.io/gitea/custom
	CustomConf    string  // 定制化配置文件路径 setting.go 506. /home/qiubing/GOPATH/src/code.gitea.io/gitea/custom/conf/app.ini
	CustomPID     string
	ProdMode      bool
	RunUser       string  // 当前登录操作系统的用户, 如 qiubing
	IsWindows     bool  // windows 平台,setting.go 413
	HasRobotsTxt  bool  // 如 false
	InternalToken string // internal access token, 如 eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOjE1NzgwNjA4MTB9.BSiCCrRILRval8NyHcCbF-iQHaTLqq5ARhSpCkAmZmY

	// UILocation is the location on the UI, so that we can display the time on UI.
	// Currently only show the default time.Local, it could be added to app.ini after UI is ready
	UILocation = time.Local
)

// DateLang transforms standard language locale name to corresponding value in datetime plugin.
func DateLang(lang string) string {
	name, ok := dateLangs[lang]
	if ok {
		return name
	}
	return "en"
}

func getAppPath() (string, error) {  // 根据 os.Args[0] 获取应用 app 绝对路径
	var appPath string
	var err error
	if IsWindows && filepath.IsAbs(os.Args[0]) {
		appPath = filepath.Clean(os.Args[0])
	} else {
		//参考:https://blog.csdn.net/u013256816/article/details/99670090.在环境变量PATH指定的目录中搜索可执行文件，如过file中有文件分隔符（斜杠），则只在当前目录搜索。
		// 即：默认在系统的环境变量里查找给定的可执行命令文件，如果查找到返回路径，否则报错??是PATH$。可提供相对路径下进行查找,并返回相对路径.
		appPath, err = exec.LookPath(os.Args[0])  // 判断 ./gitea 是否真实存在. 获取非 windows 平台中 ./gitea 的相对路径.
	}

	if err != nil {
		return "", err
	}
	appPath, err = filepath.Abs(appPath)     // 获取绝对路径. 如果传入一个相对路径就返回一个绝对路径.
	if err != nil {
		return "", err
	}
	// Note: we don't use path.Dir here because it does not handle case
	//	which path starts with two "/" in Windows: "//psf/Home/..."
	return strings.Replace(appPath, "\\", "/", -1), err
}

func getWorkPath(appPath string) string {   // 获取 app 应用的工作路径
	workPath := AppWorkPath

	if giteaWorkPath, ok := os.LookupEnv("GITEA_WORK_DIR"); ok {   // 获取系统环境变量中 'GITEA_WORK_DIR' 的值
		workPath = giteaWorkPath
	}
	if len(workPath) == 0 {  //
		i := strings.LastIndex(appPath, "/")  //获取 appPath 中最后一个 '/' 字符的下标
		if i == -1 {
			workPath = appPath
		} else {
			workPath = appPath[:i]    // 去掉最后一个 '/' 和其之后的所有字符
		}
	}
	return strings.Replace(workPath, "\\", "/", -1)
}

func init() {
	//start by qiubing
	// if os.Args[0] == "/tmp/___go_build_main_go" {
	// 	os.Args = []string {"./gitea", "web"}   // 添加这一行，因为第 415 行需要用该参数
	// }
	os.Args = []string {"./gitea", "web"}
	//os.Args = []string {"./gitea", "web"}
	//end by qiubing
	IsWindows = runtime.GOOS == "windows"  // 是不是 windows
	// We can rely on log.CanColorStdout being set properly because modules/log/console_windows.go comes before modules/setting/setting.go lexicographically
	log.NewLogger(0, "console", "console", fmt.Sprintf(`{"level": "trace", "colorize": %t, "stacktraceLevel": "none"}`, log.CanColorStdout))

	var err error
	if AppPath, err = getAppPath(); err != nil {    // 获取 app 应用的绝对路径. 如 /home/qiubing/GOPATH/src/code.gitea.io/gitea/gitea
		log.Fatal("Failed to get app path: %v", err)
	}
	AppWorkPath = getWorkPath(AppPath)   // 获取 app 应用的工作路径. 如 /home/qiubing/GOPATH/src/code.gitea.io/gitea
}

//强制路径中使用的分隔符,不能有 '\\'
func forcePathSeparator(path string) {
	if strings.Contains(path, "\\") {
		log.Fatal("Do not use '\\' or '\\\\' in paths, instead, please use '/' in all places")
	}
}

// IsRunUserMatchCurrentUser returns false if configured run user does not match
// actual user that runs the app. The first return value is the actual user name.
// This check is ignored under Windows since SSH remote login is not the main
// method to login on Windows.
// 判断配置文件中的用户是否和真实运行应用的用户相同
func IsRunUserMatchCurrentUser(runUser string) (string, bool) {
	if IsWindows || SSH.StartBuiltinServer {
		return "", true
	}

	currentUser := user.CurrentUsername()
	return currentUser, runUser == currentUser
}

func createPIDFile(pidPath string) {
	currentPid := os.Getpid()
	if err := os.MkdirAll(filepath.Dir(pidPath), os.ModePerm); err != nil {
		log.Fatal("Failed to create PID folder: %v", err)
	}

	file, err := os.Create(pidPath)
	if err != nil {
		log.Fatal("Failed to create PID file: %v", err)
	}
	defer file.Close()
	if _, err := file.WriteString(strconv.FormatInt(int64(currentPid), 10)); err != nil {
		log.Fatal("Failed to write PID information: %v", err)
	}
}

// CheckLFSVersion will check lfs version, if not satisfied, then disable it.
//检查 LFS 版本, 条件为 git 版本大于等于 2.1.2 ,不满足条件就关掉
func CheckLFSVersion() {
	if LFS.StartServer {
		//Disable LFS client hooks if installed for the current OS user
		//Needs at least git v2.1.2

		binVersion, err := git.BinVersion()   // 从 shell 中获取当前 git 版本
		if err != nil {
			log.Fatal("Error retrieving git version: %v", err)
		}

		if !version.Compare(binVersion, "2.1.2", ">=") {
			LFS.StartServer = false
			log.Error("LFS server support needs at least Git v2.1.2")
		} else {
			git.GlobalCommandArgs = append(git.GlobalCommandArgs, "-c", "filter.lfs.required=",
				"-c", "filter.lfs.smudge=", "-c", "filter.lfs.clean=")
		}
	}
}

// SetCustomPathAndConf will set CustomPath and CustomConf with reference to the
// GITEA_CUSTOM environment variable and with provided overrides before stepping
// back to the default
// 设置定制化文件夹路径和定制化配置文件路径
func SetCustomPathAndConf(providedCustom, providedConf, providedWorkPath string) {   // main.go 中传入的三个参数均为空""
	if len(providedWorkPath) != 0 {
		AppWorkPath = filepath.ToSlash(providedWorkPath)
	}
	if giteaCustom, ok := os.LookupEnv("GITEA_CUSTOM"); ok {   // 获取系统环境变量中 'GITEA_CUSTOM' 的值
		CustomPath = giteaCustom
	}
	if len(providedCustom) != 0 {
		CustomPath = providedCustom
	}
	if len(CustomPath) == 0 {
		CustomPath = path.Join(AppWorkPath, "custom")   // AppWorkPath 是在 setting.go 423 赋值的 app 应用全局变量. CustomPath 为 $AppWorkPath/custom
	} else if !filepath.IsAbs(CustomPath) {
		CustomPath = path.Join(AppWorkPath, CustomPath)
	}

	if len(providedConf) != 0 {
		CustomConf = providedConf
	}
	if len(CustomConf) == 0 {
		CustomConf = path.Join(CustomPath, "conf/app.ini")   // CustomPath 是在 setting.go 497 赋值. CustomPath 为 CustomPath/conf/app.ini
	} else if !filepath.IsAbs(CustomConf) {
		CustomConf = path.Join(CustomPath, CustomConf)
	}
}

// NewContext initializes configuration context.
// NOTE: do not print any log except error.
// 初始化配置上下文信息,加载 custom/conf/app.ini 中的配置信息
func NewContext() {
	Cfg = ini.Empty()   // 一个空的配置文件对象,用于加载 .ini 类型的配置文件

	if len(CustomPID) > 0 {
		createPIDFile(CustomPID)
	}

	if com.IsFile(CustomConf) {   // CustomConf 是不是一个文件
		if err := Cfg.Append(CustomConf); err != nil {   // 添加 CustomConf 作为一个数据源,放在 Cfg.sataSources 中.并解析 custom/conf/app.ini 中的 section 和 key
			log.Fatal("Failed to load custom conf '%s': %v", CustomConf, err)
		}
	} else {
		log.Warn("Custom config '%s' not found, ignore this if you're running first time", CustomConf)
	}
	Cfg.NameMapper = ini.AllCapsUnderscore

	homeDir, err := com.HomeDir()  // 返回系统主目录路径, 如 linux 中 /home/qiubing
	if err != nil {
		log.Fatal("Failed to get home directory: %v", err)
	}
	homeDir = strings.Replace(homeDir, "\\", "/", -1)

	//设置日志相关的配置
	LogLevel = getLogLevel(Cfg.Section("log"), "LEVEL", "Info") //Log level,默认 info
	StacktraceLogLevel = getStacktraceLogLevel(Cfg.Section("log"), "STACKTRACE_LEVEL", "None")  // 栈追踪日志级别?
	LogRootPath = Cfg.Section("log").Key("ROOT_PATH").MustString(path.Join(AppWorkPath, "log")) // log 存放根路径, $AppWorkPath/log
	forcePathSeparator(LogRootPath)   //强制路径中使用的分隔符,不能有 '\\'
	RedirectMacaronLog = Cfg.Section("log").Key("REDIRECT_MACARON_LOG").MustBool(false)  // 重定向的 macalon 日志
	RouterLogLevel = log.FromString(Cfg.Section("log").Key("ROUTER_LOG_LEVEL").MustString("Info"))  // 路由日志的级别 level, info

	//设置网路\通信相关的配置
	sec := Cfg.Section("server")   // 获取 custom/conf/app.ini 中 'server' section 的数据
	AppName = Cfg.Section("").Key("APP_NAME").MustString("Gitea: Git with a cup of tea")     // 应用名

	Protocol = HTTP
	switch sec.Key("PROTOCOL").String() {   // app.ini 中 server.PROTOCOL, 空 ""
	case "https":
		Protocol = HTTPS
		CertFile = sec.Key("CERT_FILE").String()   // app.ini 中 server.CERT_FILE, 空 ""
		KeyFile = sec.Key("KEY_FILE").String()   // app.ini 中 server.KEY_FILE, 空 ""
	case "fcgi":
		Protocol = FCGI
	case "unix":
		Protocol = UnixSocket
		UnixSocketPermissionRaw := sec.Key("UNIX_SOCKET_PERMISSION").MustString("666")
		UnixSocketPermissionParsed, err := strconv.ParseUint(UnixSocketPermissionRaw, 8, 32)
		if err != nil || UnixSocketPermissionParsed > 0777 {
			log.Fatal("Failed to parse unixSocketPermission: %s", UnixSocketPermissionRaw)
		}
		UnixSocketPermission = uint32(UnixSocketPermissionParsed)
	}
	EnableLetsEncrypt = sec.Key("ENABLE_LETSENCRYPT").MustBool(false)
	LetsEncryptTOS = sec.Key("LETSENCRYPT_ACCEPTTOS").MustBool(false)
	if !LetsEncryptTOS && EnableLetsEncrypt {
		log.Warn("Failed to enable Let's Encrypt due to Let's Encrypt TOS not being accepted")
		EnableLetsEncrypt = false
	}
	LetsEncryptDirectory = sec.Key("LETSENCRYPT_DIRECTORY").MustString("https")
	LetsEncryptEmail = sec.Key("LETSENCRYPT_EMAIL").MustString("")
	Domain = sec.Key("DOMAIN").MustString("localhost")   // 域名
	HTTPAddr = sec.Key("HTTP_ADDR").MustString("0.0.0.0")  //http 地址
	HTTPPort = sec.Key("HTTP_PORT").MustString("3000")     //http 端口

	defaultAppURL := string(Protocol) + "://" + Domain  //默认应用 URL
	if (Protocol == HTTP && HTTPPort != "80") || (Protocol == HTTPS && HTTPPort != "443") {   // 给 URL 添加端口
		defaultAppURL += ":" + HTTPPort
	}
	AppURL = sec.Key("ROOT_URL").MustString(defaultAppURL)
	AppURL = strings.TrimRight(AppURL, "/") + "/"

	// Check if has app suburl.
	appURL, err := url.Parse(AppURL)  // 判断应用 url 是否有子 url
	if err != nil {
		log.Fatal("Invalid ROOT_URL '%s': %s", AppURL, err)
	}
	// Suburl should start with '/' and end without '/', such as '/{subpath}'.
	// This value is empty if site does not have sub-url.
	AppSubURL = strings.TrimSuffix(appURL.Path, "/")   // 删除 appURL.Path 尾部的 '/' 字符串
	AppSubURLDepth = strings.Count(AppSubURL, "/")    // 计算 AppSubURL 中 '/' 字符串的数量
	// Check if Domain differs from AppURL domain than update it to AppURL's domain
	// TODO: Can be replaced with url.Hostname() when minimal GoLang version is 1.8
	urlHostname := strings.SplitN(appURL.Host, ":", 2)[0]   // url主机名,如 localhost
	if urlHostname != Domain && net.ParseIP(urlHostname) == nil {   // 更换全局域名
		Domain = urlHostname
	}

	var defaultLocalURL string   // http://localhost:3000/
	switch Protocol {
	case UnixSocket:
		defaultLocalURL = "http://unix/"
	case FCGI:
		defaultLocalURL = AppURL
	default:
		defaultLocalURL = string(Protocol) + "://"
		if HTTPAddr == "0.0.0.0" {
			defaultLocalURL += "localhost"
		} else {
			defaultLocalURL += HTTPAddr
		}
		defaultLocalURL += ":" + HTTPPort + "/"
	}
	LocalURL = sec.Key("LOCAL_ROOT_URL").MustString(defaultLocalURL)
	RedirectOtherPort = sec.Key("REDIRECT_OTHER_PORT").MustBool(false)
	PortToRedirect = sec.Key("PORT_TO_REDIRECT").MustString("80")
	OfflineMode = sec.Key("OFFLINE_MODE").MustBool()
	DisableRouterLog = sec.Key("DISABLE_ROUTER_LOG").MustBool()
	StaticRootPath = sec.Key("STATIC_ROOT_PATH").MustString(AppWorkPath)
	AppDataPath = sec.Key("APP_DATA_PATH").MustString(path.Join(AppWorkPath, "data"))
	EnableGzip = sec.Key("ENABLE_GZIP").MustBool()
	EnablePprof = sec.Key("ENABLE_PPROF").MustBool(false)
	PprofDataPath = sec.Key("PPROF_DATA_PATH").MustString(path.Join(AppWorkPath, "data/tmp/pprof"))
	if !filepath.IsAbs(PprofDataPath) {   // 判断 PprofDataPath 是否为绝对路径
		PprofDataPath = filepath.Join(AppWorkPath, PprofDataPath)
	}

	switch sec.Key("LANDING_PAGE").MustString("home") {    // 默认 landing_page 主页 home "/"
	case "explore":
		LandingPageURL = LandingPageExplore
	case "organizations":
		LandingPageURL = LandingPageOrganizations
	default:
		LandingPageURL = LandingPageHome
	}

	//shh 相关内容配置
	if len(SSH.Domain) == 0 {
		SSH.Domain = Domain
	}
	SSH.RootPath = path.Join(homeDir, ".ssh")   // 如 /home/qiubing/.ssh
	serverCiphers := sec.Key("SSH_SERVER_CIPHERS").Strings(",")
	if len(serverCiphers) > 0 {
		SSH.ServerCiphers = serverCiphers
	}
	serverKeyExchanges := sec.Key("SSH_SERVER_KEY_EXCHANGES").Strings(",")
	if len(serverKeyExchanges) > 0 {
		SSH.ServerKeyExchanges = serverKeyExchanges
	}
	serverMACs := sec.Key("SSH_SERVER_MACS").Strings(",")
	if len(serverMACs) > 0 {
		SSH.ServerMACs = serverMACs
	}
	SSH.KeyTestPath = os.TempDir()   // os.TempDir 获取临时文件存放路径,unix 中 $TMPDIR 环境变量的值,如果环境变量干部存在则默认 /tmp
	if err = Cfg.Section("server").MapTo(&SSH); err != nil {
		log.Fatal("Failed to map SSH settings: %v", err)
	}

	SSH.KeygenPath = sec.Key("SSH_KEYGEN_PATH").MustString("ssh-keygen")
	SSH.Port = sec.Key("SSH_PORT").MustInt(22)
	SSH.ListenPort = sec.Key("SSH_LISTEN_PORT").MustInt(SSH.Port)

	// When disable SSH, start builtin server value is ignored.
	if SSH.Disabled {
		SSH.StartBuiltinServer = false
	}

	if !SSH.Disabled && !SSH.StartBuiltinServer {
		if err := os.MkdirAll(SSH.RootPath, 0700); err != nil {
			log.Fatal("Failed to create '%s': %v", SSH.RootPath, err)
		} else if err = os.MkdirAll(SSH.KeyTestPath, 0644); err != nil {
			log.Fatal("Failed to create '%s': %v", SSH.KeyTestPath, err)
		}
	}

	SSH.MinimumKeySizeCheck = sec.Key("MINIMUM_KEY_SIZE_CHECK").MustBool()
	SSH.MinimumKeySizes = map[string]int{}
	minimumKeySizes := Cfg.Section("ssh.minimum_key_sizes").Keys()
	for _, key := range minimumKeySizes {
		if key.MustInt() != -1 {
			SSH.MinimumKeySizes[strings.ToLower(key.Name())] = key.MustInt()
		}
	}
	SSH.AuthorizedKeysBackup = sec.Key("SSH_AUTHORIZED_KEYS_BACKUP").MustBool(true)
	SSH.CreateAuthorizedKeysFile = sec.Key("SSH_CREATE_AUTHORIZED_KEYS_FILE").MustBool(true)
	SSH.ExposeAnonymous = sec.Key("SSH_EXPOSE_ANONYMOUS").MustBool(false)

	//LFS 相关内容配置
	sec = Cfg.Section("server")
	if err = sec.MapTo(&LFS); err != nil {      // sec.MapTo(&LFS) 将 sec 中的内容复制给 LFS
		log.Fatal("Failed to map LFS settings: %v", err)
	}
	LFS.ContentPath = sec.Key("LFS_CONTENT_PATH").MustString(filepath.Join(AppDataPath, "lfs"))  // 如 /home/qiubing/GOPATH/src/code.gitea.io/gitea/data/lfs
	if !filepath.IsAbs(LFS.ContentPath) {
		LFS.ContentPath = filepath.Join(AppWorkPath, LFS.ContentPath)
	}

	LFS.HTTPAuthExpiry = sec.Key("LFS_HTTP_AUTH_EXPIRY").MustDuration(20 * time.Minute)

	if LFS.StartServer {
		if err := os.MkdirAll(LFS.ContentPath, 0700); err != nil {
			log.Fatal("Failed to create '%s': %v", LFS.ContentPath, err)
		}

		LFS.JWTSecretBytes = make([]byte, 32)
		n, err := base64.RawURLEncoding.Decode(LFS.JWTSecretBytes, []byte(LFS.JWTSecretBase64))   // 长度为 32 的 []uint8,元素全为 0
		if err != nil || n != 32 {
			LFS.JWTSecretBase64, err = generate.NewJwtSecret()
			if err != nil {
				log.Fatal("Error generating JWT Secret for custom config: %v", err)
				return
			}

			// Save secret
			cfg := ini.Empty()
			if com.IsFile(CustomConf) {
				// Keeps custom settings if there is already something.
				if err := cfg.Append(CustomConf); err != nil {
					log.Error("Failed to load custom conf '%s': %v", CustomConf, err)
				}
			}

			cfg.Section("server").Key("LFS_JWT_SECRET").SetValue(LFS.JWTSecretBase64)

			if err := os.MkdirAll(filepath.Dir(CustomConf), os.ModePerm); err != nil {
				log.Fatal("Failed to create '%s': %v", CustomConf, err)
			}
			if err := cfg.SaveTo(CustomConf); err != nil {
				log.Fatal("Error saving generated JWT Secret to custom config: %v", err)
				return
			}
		}
	}

	//OAuth2 相关内容初始化
	if err = Cfg.Section("oauth2").MapTo(&OAuth2); err != nil {   // Cfg.Section("oauth2").MapTo(&OAuth2) 将 oauth2 章节的内容复制给全局变量 OAuth2
		log.Fatal("Failed to OAuth2 settings: %v", err)
		return
	}

	if OAuth2.Enable {
		OAuth2.JWTSecretBytes = make([]byte, 32)
		n, err := base64.RawURLEncoding.Decode(OAuth2.JWTSecretBytes, []byte(OAuth2.JWTSecretBase64))

		if err != nil || n != 32 {
			OAuth2.JWTSecretBase64, err = generate.NewJwtSecret()
			if err != nil {
				log.Fatal("error generating JWT secret: %v", err)
				return
			}
			cfg := ini.Empty()
			if com.IsFile(CustomConf) {
				if err := cfg.Append(CustomConf); err != nil {
					log.Error("failed to load custom conf %s: %v", CustomConf, err)
					return
				}
			}
			cfg.Section("oauth2").Key("JWT_SECRET").SetValue(OAuth2.JWTSecretBase64)

			if err := os.MkdirAll(filepath.Dir(CustomConf), os.ModePerm); err != nil {
				log.Fatal("failed to create '%s': %v", CustomConf, err)
				return
			}
			if err := cfg.SaveTo(CustomConf); err != nil {
				log.Fatal("error saving generating JWT secret to custom config: %v", err)
				return
			}
		}
	}

	// admin 相关内容初始化
	sec = Cfg.Section("admin")
	Admin.DefaultEmailNotification = sec.Key("DEFAULT_EMAIL_NOTIFICATIONS").MustString("enabled")   // 默认邮件通知 enabled

	//security 相关内容初始化
	sec = Cfg.Section("security")
	InstallLock = sec.Key("INSTALL_LOCK").MustBool(false)
	SecretKey = sec.Key("SECRET_KEY").MustString("!#@FDEWREWR&*(")
	LogInRememberDays = sec.Key("LOGIN_REMEMBER_DAYS").MustInt(7)
	CookieUserName = sec.Key("COOKIE_USERNAME").MustString("gitea_awesome")
	CookieRememberName = sec.Key("COOKIE_REMEMBER_NAME").MustString("gitea_incredible")
	ReverseProxyAuthUser = sec.Key("REVERSE_PROXY_AUTHENTICATION_USER").MustString("X-WEBAUTH-USER")
	ReverseProxyAuthEmail = sec.Key("REVERSE_PROXY_AUTHENTICATION_EMAIL").MustString("X-WEBAUTH-EMAIL")
	MinPasswordLength = sec.Key("MIN_PASSWORD_LENGTH").MustInt(6)
	ImportLocalPaths = sec.Key("IMPORT_LOCAL_PATHS").MustBool(false)
	DisableGitHooks = sec.Key("DISABLE_GIT_HOOKS").MustBool(false)
	PasswordHashAlgo = sec.Key("PASSWORD_HASH_ALGO").MustString("pbkdf2")
	CSRFCookieHTTPOnly = sec.Key("CSRF_COOKIE_HTTP_ONLY").MustBool(true)

	InternalToken = loadInternalToken(sec)

	cfgdata := sec.Key("PASSWORD_COMPLEXITY").Strings(",")
	PasswordComplexity = make([]string, 0, len(cfgdata))
	for _, name := range cfgdata {
		name := strings.ToLower(strings.Trim(name, `"`))
		if name != "" {
			PasswordComplexity = append(PasswordComplexity, name)
		}
	}

	//attachment 相关内容初始化
	sec = Cfg.Section("attachment")
	AttachmentPath = sec.Key("PATH").MustString(path.Join(AppDataPath, "attachments"))  // 如 /home/qiubing/GOPATH/src/code.gitea.io/gitea/data/attachments
	if !filepath.IsAbs(AttachmentPath) {
		AttachmentPath = path.Join(AppWorkPath, AttachmentPath)
	}
	AttachmentAllowedTypes = strings.Replace(sec.Key("ALLOWED_TYPES").MustString("image/jpeg,image/png,application/zip,application/gzip"), "|", ",", -1)
	AttachmentMaxSize = sec.Key("MAX_SIZE").MustInt64(4)
	AttachmentMaxFiles = sec.Key("MAX_FILES").MustInt(5)
	AttachmentEnabled = sec.Key("ENABLED").MustBool(true)

	//time 相关内容初始化
	timeFormatKey := Cfg.Section("time").Key("FORMAT").MustString("")
	if timeFormatKey != "" {
		TimeFormat = map[string]string{
			"ANSIC":       time.ANSIC,
			"UnixDate":    time.UnixDate,
			"RubyDate":    time.RubyDate,
			"RFC822":      time.RFC822,
			"RFC822Z":     time.RFC822Z,
			"RFC850":      time.RFC850,
			"RFC1123":     time.RFC1123,
			"RFC1123Z":    time.RFC1123Z,
			"RFC3339":     time.RFC3339,
			"RFC3339Nano": time.RFC3339Nano,
			"Kitchen":     time.Kitchen,
			"Stamp":       time.Stamp,
			"StampMilli":  time.StampMilli,
			"StampMicro":  time.StampMicro,
			"StampNano":   time.StampNano,
		}[timeFormatKey]
		// When the TimeFormatKey does not exist in the previous map e.g.'2006-01-02 15:04:05'
		if len(TimeFormat) == 0 {
			TimeFormat = timeFormatKey
			TestTimeFormat, _ := time.Parse(TimeFormat, TimeFormat)
			if TestTimeFormat.Format(time.RFC3339) != "2006-01-02T15:04:05Z" {
				log.Fatal("Can't create time properly, please check your time format has 2006, 01, 02, 15, 04 and 05")
			}
			log.Trace("Custom TimeFormat: %s", TimeFormat)
		}
	}

	zone := Cfg.Section("time").Key("DEFAULT_UI_LOCATION").String()
	if zone != "" {
		DefaultUILocation, err = time.LoadLocation(zone)
		if err != nil {
			log.Fatal("Load time zone failed: %v", err)
		} else {
			log.Info("Default UI Location is %v", zone)
		}
	}
	if DefaultUILocation == nil {
		DefaultUILocation = time.Local
	}

	RunUser = Cfg.Section("").Key("RUN_USER").MustString(user.CurrentUsername())  // user.CurrentUsername() 获取当前登录操作系统的用户, 如 qiubing
	// Does not check run user when the install lock is off.
	if InstallLock {
		currentUser, match := IsRunUserMatchCurrentUser(RunUser)
		if !match {
			log.Fatal("Expect user '%s' but current user is: %s", RunUser, currentUser)
		}
	}

	SSH.BuiltinServerUser = Cfg.Section("server").Key("BUILTIN_SSH_SERVER_USER").MustString(RunUser)   // 编译服务的用户
	//初始化和 repository 相关的全局配置, 配置的全局变量位于 modules/setting/repository.go 中
	newRepository()

	//picture\头像 相关内容初始化
	sec = Cfg.Section("picture")
	AvatarUploadPath = sec.Key("AVATAR_UPLOAD_PATH").MustString(path.Join(AppDataPath, "avatars"))  // 头像上传的路径
	forcePathSeparator(AvatarUploadPath)  // 检查分隔符
	if !filepath.IsAbs(AvatarUploadPath) {
		AvatarUploadPath = path.Join(AppWorkPath, AvatarUploadPath)
	}
	RepositoryAvatarUploadPath = sec.Key("REPOSITORY_AVATAR_UPLOAD_PATH").MustString(path.Join(AppDataPath, "repo-avatars"))  // 仓库头像上传路径
	forcePathSeparator(RepositoryAvatarUploadPath)
	if !filepath.IsAbs(RepositoryAvatarUploadPath) {
		RepositoryAvatarUploadPath = path.Join(AppWorkPath, RepositoryAvatarUploadPath)
	}
	RepositoryAvatarFallback = sec.Key("REPOSITORY_AVATAR_FALLBACK").MustString("none")
	RepositoryAvatarFallbackImage = sec.Key("REPOSITORY_AVATAR_FALLBACK_IMAGE").MustString("/img/repo_default.png")
	AvatarMaxWidth = sec.Key("AVATAR_MAX_WIDTH").MustInt(4096)
	AvatarMaxHeight = sec.Key("AVATAR_MAX_HEIGHT").MustInt(3072)
	AvatarMaxFileSize = sec.Key("AVATAR_MAX_FILE_SIZE").MustInt64(1048576)
	switch source := sec.Key("GRAVATAR_SOURCE").MustString("gravatar"); source {
	case "duoshuo":
		GravatarSource = "http://gravatar.duoshuo.com/avatar/"
	case "gravatar":
		GravatarSource = "https://secure.gravatar.com/avatar/"   // 头像源
	case "libravatar":
		GravatarSource = "https://seccdn.libravatar.org/avatar/"
	default:
		GravatarSource = source
	}
	DisableGravatar = sec.Key("DISABLE_GRAVATAR").MustBool()
	EnableFederatedAvatar = sec.Key("ENABLE_FEDERATED_AVATAR").MustBool(!InstallLock)
	if OfflineMode {
		DisableGravatar = true
		EnableFederatedAvatar = false
	}
	if DisableGravatar {
		EnableFederatedAvatar = false
	}
	if EnableFederatedAvatar || !DisableGravatar {
		GravatarSourceURL, err = url.Parse(GravatarSource)
		if err != nil {
			log.Fatal("Failed to parse Gravatar URL(%s): %v",
				GravatarSource, err)
		}
	}

	if EnableFederatedAvatar {
		LibravatarService = libravatar.New()
		if GravatarSourceURL.Scheme == "https" {
			LibravatarService.SetUseHTTPS(true)
			LibravatarService.SetSecureFallbackHost(GravatarSourceURL.Host)
		} else {
			LibravatarService.SetUseHTTPS(false)
			LibravatarService.SetFallbackHost(GravatarSourceURL.Host)
		}
	}

	//ui 相关内容初始化
	if err = Cfg.Section("ui").MapTo(&UI); err != nil {    // 映射
		log.Fatal("Failed to map UI settings: %v", err)
	} else if err = Cfg.Section("markdown").MapTo(&Markdown); err != nil {
		log.Fatal("Failed to map Markdown settings: %v", err)
	} else if err = Cfg.Section("admin").MapTo(&Admin); err != nil {
		log.Fatal("Fail to map Admin settings: %v", err)
	} else if err = Cfg.Section("api").MapTo(&API); err != nil {
		log.Fatal("Failed to map API settings: %v", err)
	} else if err = Cfg.Section("metrics").MapTo(&Metrics); err != nil {
		log.Fatal("Failed to map Metrics settings: %v", err)
	}

	u := *appURL
	u.Path = path.Join(u.Path, "api", "swagger")   // 如 /api/swagger
	API.SwaggerURL = u.String()    // 如 http://localhost:3000/api/swagger

	// core 相关内容的全局变量初始化
	newCron()
	// git 相关内容的全局变量初始化
	newGit()

	//mirror 相关内容的全局变量初始化
	sec = Cfg.Section("mirror")
	Mirror.MinInterval = sec.Key("MIN_INTERVAL").MustDuration(10 * time.Minute)
	Mirror.DefaultInterval = sec.Key("DEFAULT_INTERVAL").MustDuration(8 * time.Hour)
	if Mirror.MinInterval.Minutes() < 1 {
		log.Warn("Mirror.MinInterval is too low")
		Mirror.MinInterval = 1 * time.Minute
	}
	if Mirror.DefaultInterval < Mirror.MinInterval {
		log.Warn("Mirror.DefaultInterval is less than Mirror.MinInterval")
		Mirror.DefaultInterval = time.Hour * 8
	}

	//i18n 相关内容的全局变量初始化
	Langs = Cfg.Section("i18n").Key("LANGS").Strings(",")
	if len(Langs) == 0 {
		Langs = []string{
			"en-US", "zh-CN", "zh-HK", "zh-TW", "de-DE", "fr-FR", "nl-NL", "lv-LV",
			"ru-RU", "uk-UA", "ja-JP", "es-ES", "pt-BR", "pl-PL", "bg-BG", "it-IT",
			"fi-FI", "tr-TR", "cs-CZ", "sr-SP", "sv-SE", "ko-KR"}
	}
	Names = Cfg.Section("i18n").Key("NAMES").Strings(",")
	if len(Names) == 0 {
		Names = []string{"English", "简体中文", "繁體中文（香港）", "繁體中文（台灣）", "Deutsch",
			"français", "Nederlands", "latviešu", "русский", "Українська", "日本語",
			"español", "português do Brasil", "polski", "български", "italiano",
			"suomi", "Türkçe", "čeština", "српски", "svenska", "한국어"}
	}
	dateLangs = Cfg.Section("i18n.datelang").KeysHash()

	ShowFooterBranding = Cfg.Section("other").Key("SHOW_FOOTER_BRANDING").MustBool(false)
	ShowFooterVersion = Cfg.Section("other").Key("SHOW_FOOTER_VERSION").MustBool(true)
	ShowFooterTemplateLoadTime = Cfg.Section("other").Key("SHOW_FOOTER_TEMPLATE_LOAD_TIME").MustBool(true)

	UI.ShowUserEmail = Cfg.Section("ui").Key("SHOW_USER_EMAIL").MustBool(true)
	UI.DefaultShowFullName = Cfg.Section("ui").Key("DEFAULT_SHOW_FULL_NAME").MustBool(false)
	UI.SearchRepoDescription = Cfg.Section("ui").Key("SEARCH_REPO_DESCRIPTION").MustBool(true)

	HasRobotsTxt = com.IsFile(path.Join(CustomPath, "robots.txt"))

	//markup 相关内容的全局变量初始化
	newMarkup()

	//U2F 相关内容的全局变量初始化
	sec = Cfg.Section("U2F")
	U2F.TrustedFacets, _ = shellquote.Split(sec.Key("TRUSTED_FACETS").MustString(strings.TrimRight(AppURL, "/")))
	U2F.AppID = sec.Key("APP_ID").MustString(strings.TrimRight(AppURL, "/"))

	zip.Verbose = false
}

//加载 internal token
func loadInternalToken(sec *ini.Section) string {
	uri := sec.Key("INTERNAL_TOKEN_URI").String()
	if len(uri) == 0 {
		return loadOrGenerateInternalToken(sec)
	}
	tempURI, err := url.Parse(uri)
	if err != nil {
		log.Fatal("Failed to parse INTERNAL_TOKEN_URI (%s): %v", uri, err)
	}
	switch tempURI.Scheme {
	case "file":
		fp, err := os.OpenFile(tempURI.RequestURI(), os.O_RDWR, 0600)
		if err != nil {
			log.Fatal("Failed to open InternalTokenURI (%s): %v", uri, err)
		}
		defer fp.Close()

		buf, err := ioutil.ReadAll(fp)
		if err != nil {
			log.Fatal("Failed to read InternalTokenURI (%s): %v", uri, err)
		}
		// No token in the file, generate one and store it.
		if len(buf) == 0 {
			token, err := generate.NewInternalToken()
			if err != nil {
				log.Fatal("Error generate internal token: %v", err)
			}
			if _, err := io.WriteString(fp, token); err != nil {
				log.Fatal("Error writing to InternalTokenURI (%s): %v", uri, err)
			}
			return token
		}

		return string(buf)
	default:
		log.Fatal("Unsupported URI-Scheme %q (INTERNAL_TOKEN_URI = %q)", tempURI.Scheme, uri)
	}
	return ""
}

func loadOrGenerateInternalToken(sec *ini.Section) string {
	var err error
	token := sec.Key("INTERNAL_TOKEN").String()
	if len(token) == 0 {
		token, err = generate.NewInternalToken()
		if err != nil {
			log.Fatal("Error generate internal token: %v", err)
		}

		// Save secret
		cfgSave := ini.Empty()
		if com.IsFile(CustomConf) {
			// Keeps custom settings if there is already something.
			if err := cfgSave.Append(CustomConf); err != nil {
				log.Error("Failed to load custom conf '%s': %v", CustomConf, err)
			}
		}

		cfgSave.Section("security").Key("INTERNAL_TOKEN").SetValue(token)

		if err := os.MkdirAll(filepath.Dir(CustomConf), os.ModePerm); err != nil {
			log.Fatal("Failed to create '%s': %v", CustomConf, err)
		}
		if err := cfgSave.SaveTo(CustomConf); err != nil {
			log.Fatal("Error saving generated INTERNAL_TOKEN to custom config: %v", err)
		}
	}
	return token
}

// NewServices initializes the services
//初始化服务
func NewServices() {
	InitDBConfig()  // 初始化数据库配置信息
	newService()  // 初始化 service 配置信息
	NewLogServices(false)   // 初始化各种日志服务
	newCacheService()  //初始化 cache 缓存服务
	newSessionService()  //初始化 session 会话服务
	newCORSService()  //初始化 cors 服务配置, Cross-Origin Resource Sharing 是一种跨域资源贡献服务
	newMailService()  //初始化邮件配置服务
	newRegisterMailService()  //初始化注册人邮件配置服务
	newNotifyMailService()  //初始化邮件通知服务配置
	newWebhookService()  //初始化 web 钩子服务配置
	newIndexerService()  // 初始化搜索索引服务配置?
	newTaskService()  // 初始化任务服务配置?
}
